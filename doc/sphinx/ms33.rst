.. _ms33:

Milestone 33: workplan definition
=================================

This document will describe the work done under the PRACE-4IP extension. This
task is dedicated to provide useful information on application *performance and
energy usage* on next generation systems on the path towards exacsale. It will
be caried out running the accelerated UEABS on PCP systems to obtain energy metrics
on *OpenPower+GPU, Xeon Phi and FPGA*.

PCP systems availables
----------------------

This section describes the systems where codes owners have been granted access.
The table :ref:`table-pcp-systems` sums up systems and availability:

.. _table-pcp-systems:
.. table:: PCP Systems
   :widths: auto

   +--------------+--------------+----------------------------+---------------+-------------------------------+
   |  Technology  | Theoretical  | Manufacturer               | Host          | Availability                  |
   |              | peak perf    |                            |               |                               |
   +==============+==============+============================+===============+===============================+
   | Power8 + GPU | 877 TFlop/s  | `E4 computer engineering`_ | CINECA_ (It)  | June/July 2017                |
   |              |              |                            |               | **shifted to mid-October**    |
   +--------------+--------------+----------------------------+---------------+-------------------------------+
   | Xeon Phi     | 512 TFlop/s  | `Atos/Bull`_               | CINES_ (Fr)   | June 2017 (now available)     |
   +--------------+--------------+----------------------------+---------------+-------------------------------+
   | FPGA         | N/A          | MAXELER_                   | JSC_ (De)     | August 2017                   |
   |              |              |                            |               | **shifted to mid-October**    |
   +--------------+--------------+----------------------------+---------------+-------------------------------+

.. note:: More detailed information can be found for :ref:`e4_gpu`, :ref:`atos_knl`
          and :ref:`maxeler_fpga` systems. It includes, hardware description,
          registration procedures, and energy hardware and tool information.



Code definition
---------------

Two sets of codes will be run. One will focus on giving metrics on multiple machines
for UEABS codes while the other will focus on porting specific kernels to the KNL
machine.

UEABS
^^^^^

The table :ref:`table-code-definition` shows all codes available with UEABS
(regular and accelerated). It states for each codes, tageted architechures and BCOs.
Note that due to tight deadlines, efforts to port codes to new architechures will
have to be minimal.

.. _table-code-definition:
.. table:: Code definition
   :widths: auto

   +------------------------+--------------------------------+-------------------------------+
   |                        |           Will run on          |                               |
   |   Code name            +--------------+----------+------+   4IP-extension BCO           +
   |                        | Power8 + GPU | Xeon Phi | FPGA |                               |
   +========================+==============+==========+======+===============================+
   | ALYA                   | ✓            | ✓        | ✗    | Ricard Borrell (BSC)          |
   +------------------------+--------------+----------+------+-------------------------------+
   | Code_Saturne           | ✓            | ✓        | ✗    | Charles Moulinec (STFC)       |
   +------------------------+--------------+----------+------+-------------------------------+
   | CP2K                   | ✓            | ✓        | ✗    | Arno Proeme (EPCC)            |
   +------------------------+--------------+----------+------+-------------------------------+
   | GADGET                 | ✗            | ✓        | ✗    | Volker Weinberg (LRZ)         |
   +------------------------+--------------+----------+------+-------------------------------+
   | GENE                   | ✗            | ✗        | ✗    | ✗                             |
   +------------------------+--------------+----------+------+-------------------------------+
   | GPAW                   | ✗            | ✓        | ✗    | Martti Louhivuori (CSC)       |
   +------------------------+--------------+----------+------+-------------------------------+
   | GROMACS                | ✓            | ✓        | ✗    | Dimitris Dellis (GRNET)       |
   +------------------------+--------------+----------+------+-------------------------------+
   | NAMD                   | ✓            | ✓        | ✗    | Dimitris Dellis (GRNET)       |
   +------------------------+--------------+----------+------+-------------------------------+
   | NEMO                   | ✗            | ✓        | ✗    | Arno Proeme (EPCC)            |
   +------------------------+--------------+----------+------+-------------------------------+
   | PFARM                  | ✓            | ✓        | ✗    | Mariusz Uchronski (WCNS/PSNC) |
   +------------------------+--------------+----------+------+-------------------------------+
   | QCD                    | ✓            | ✓        | ✗    | Jacob Finkenrath (CyI)        |
   +------------------------+--------------+----------+------+-------------------------------+
   | Quantum Espresso       | ✓            | ✓        | ✓    | Andrew Emerson (CINECA)       |
   +------------------------+--------------+----------+------+-------------------------------+
   | SHOC                   | ✓            | ✗        | ✓    | Valeriu Codreanu (SurfSARA)   |
   +------------------------+--------------+----------+------+-------------------------------+
   | Specfem3D_Globe        | ✓            | ✓        | ✓    | Victor Cameo Ponz (CINES)     |
   +------------------------+--------------+----------+------+-------------------------------+

.. note:: Code descriptions are available on the `Description of the initial
          accelerator benchmark suite` and on the `UEABS description web page`_.

.. _inria_plan:

Energy profiling of the HORSE+MaPHyS+PaStiX stack
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The work will aim at porting the HORSE+MaPHyS+PaStiX solver stack on the KNL-based system.
It will consists in performing an energetic profiling of theses codes and studying the influence of several parameters
driving the accuracy and numerical efficiency of the underlying simulations.
A parametric study for minimizing the energy consumption will be performed.
The deliverable will include details of this parametric study and a discussion of its main results.

.. note:: More can be found on the `HORSE software page`_.


.. _Description of the initial accelerator benchmark suite: http://www.prace-ri.eu/IMG/pdf/WP212.pdf
.. _UEABS description web page: http://www.prace-ri.eu/ueabs/
.. _HORSE software page: http://www-sop.inria.fr/nachos/index.php/Software/HORSE


.. _MAXELER: http://maxeler.com/
.. _JSC: http://www.fz-juelich.de/ias/jsc/EN/Home/home_node.html
.. _E4 computer engineering: https://www.e4company.com
.. _CINECA: http://hpc.cineca.it/
.. _Atos/Bull: https://bull.com/
.. _CINES: https://www.cines.fr/

.. _Slurm: https://slurm.schedmd.com/
