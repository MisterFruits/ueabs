#ifndef _LATTICE_H
#define _LATTICE_H
/****************************** lattice.h ********************************/

/* include file for MIMD version 6
   This file defines global scalars and the fields in the lattice. */

#include "macros.h"	/* For MAXFILENAME */
#include "random.h"	/* For double_prn */
#include "io_lat.h"	/* For gauge_file */

#include "su3.h"
#include "random.h"	/* For double_prn */

/* The lattice is an array of sites.  */
typedef struct
{
    short x, y, z, t;
    char parity;
    int index;
} site;


/* End definition of site structure */

/* Definition of globals */

#ifdef CONTROL
#define EXTERN
#else
#define EXTERN extern
#endif

/* fields */
EXTERN half_wilson_vector *htmp[8]; /* temporary arrays for dslash */
EXTERN float *htmp_32[8];
EXTERN su3_matrix *gauge;
EXTERN float *gauge_32;

/* global scalars */
EXTERN int startflag;		/* beginning lattice: CONTINUE, RELOAD, FRESH */
EXTERN char conf_id[256];
EXTERN int nx, ny, nz, nt;	/* lattice dimensions */
EXTERN int volume;		/* volume of lattice = nx*ny*nz*nt */
EXTERN int verbose;		/* for verbose printf */
EXTERN int niter;
EXTERN double rsqmin;

/* timing, memory size */
EXTERN double time_comm;
EXTERN double memsize;
/* on fly results to output1 */
EXTERN FILE *file_o1;

/* analyze */
EXTERN double mass_wilson, kappa;
EXTERN double ov_prec;
EXTERN int n_hyp_smear;
EXTERN int st_step;
EXTERN double st_rho;
EXTERN int spect_nmass;
EXTERN double *spect_mass;
EXTERN int spect_ntslice, *spect_tslice;
EXTERN int spect_allspinor, spect_spinor;

/* Some of these global variables are node dependent */
/* They are set in "make_lattice()" */
EXTERN int sites_on_node;	/* number of sites on this node */
EXTERN int even_sites_on_node;	/* number of even sites on this node */
EXTERN int odd_sites_on_node;	/* number of odd sites on this node */
EXTERN int number_of_nodes;	/* number of nodes in use */
EXTERN int this_node;		/* node number of this node */

/* The lattice is a single global variable - (actually this is the
   part of the lattice on this node) */
EXTERN site *lattice;

/* Vectors for addressing */
/* Generic pointers, for gather routines */
#define N_POINTERS 8
EXTERN char **gen_pt[N_POINTERS];

EXTERN int debugflag;

EXTERN int max_cg_iters;
#endif /* _LATTICE_H */
